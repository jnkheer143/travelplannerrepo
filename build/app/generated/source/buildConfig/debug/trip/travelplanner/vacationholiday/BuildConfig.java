/**
 * Automatically generated file. DO NOT MODIFY
 */
package trip.travelplanner.vacationholiday;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "trip.travelplanner.vacationholiday";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 15;
  public static final String VERSION_NAME = "3.1";
}
