import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:onboarding_flow/Model/CityModel.dart';
import 'package:onboarding_flow/business/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:onboarding_flow/ui/intro/welcome_screen.dart';
import 'package:http/http.dart' as http;
import 'package:onboarding_flow/utils/app_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../video/BackgroundVideo.dart';
import '../feedback/Feedback.dart';
import 'Topics.dart';
import '../savedTrips/saved_trips_screen.dart';

class NewHome extends StatefulWidget {
  bool showlogout;

  NewHome(bool bool) {
    showlogout = bool;
  }

  _NewHomeState createState() => _NewHomeState();
}

class _NewHomeState extends State<NewHome> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    savebool().then((value) => null);
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          actions: <Widget>[appLogo()],
          elevation: 0.5,
          leading: new IconButton(
              icon: new Icon(Icons.menu),
              onPressed: () => _scaffoldKey.currentState.openDrawer()),
          title: Text("Travel Planner"),
          centerTitle: true,
        ),
        drawer: Drawer(
          child: drawerUI(context),
        ),
        body: Topics());
  }

  Future<String> getUID() async {
    final FirebaseUser u = await FirebaseAuth.instance.currentUser();

    return u.uid;
  }


  Future<String> savebool() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('seen', true);
    return "true";
  }

  Future<CityModel> fetchAlbum() async {
    final response =
        await http.get('https://jsonplaceholder.typicode.com/albums/1');

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return CityModel.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  void _logOut() async {
    Auth.signOut();
  }

  appLogo() {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: SizedBox(
        width: 40,
        height: 40,
        child: Image(
          image: AssetImage("assets/newicon.png"),
        ),
      ),
    );
  }

  drawerUI(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration: BoxDecoration(color: Colors.white),
          currentAccountPicture: new CircleAvatar(
            radius: 50.0,
            backgroundColor: const Color(0xFF778899),
            backgroundImage: AssetImage("assets/newicon.png"),
          ),
        ),
//            AssetImage("assets/newicon.png")
        ListTile(
          title: Text('Saved trips'),
          onTap: () {
            AppUtils.nextScreen(context, UserLike());
          },
        ),
        ListTile(
          title: Text('Video Tutorial'),
          onTap: () {
            AppUtils.nextScreen(context, BackgroundVideo());

          },
        ),
        ListTile(
          title: Text('Feedback'),
          onTap: () {
            AppUtils.nextScreen(context, UserFeedback());

          },
        ),
        widget.showlogout
            ? ListTile(
                title: Text('Log out'),
                onTap: () async {
                  _logOut();
                  AppUtils.nextScreen(context, WelcomeScreen());
                },
              )
            : ListTile(
                title: Text('Log in'),
                onTap: () async {
                  Navigator.of(context).pushNamed("/signin");
                },
              )
      ],
    );
  }
}
