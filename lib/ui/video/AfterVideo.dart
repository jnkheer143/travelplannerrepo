import 'dart:async';
import 'package:flutter/material.dart';
import 'package:location/location.dart' as LocationManager;

import '../home/NewHome.dart';

class AfterVideo extends StatefulWidget {
  @override
  _beforeVideoState createState() => _beforeVideoState();
}

class _beforeVideoState extends State<AfterVideo> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          color: Colors.blue,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(38.0),
              child: Text(
                "Thanks!..App is starting now...",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 35, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 2000), () {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => NewHome(false)));
    });
  }
}
