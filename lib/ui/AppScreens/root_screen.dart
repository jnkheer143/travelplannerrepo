import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:onboarding_flow/ui/intro/welcome_screen.dart';

import '../home/NewHome.dart';

class RootScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _RootScreenState();
}

class _RootScreenState extends State<RootScreen> {
  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return new Container(
            color:  Color(0xff6495ED),
          );
        } else {
          if (snapshot.hasData) {
            return new NewHome(
             true
            );
          } else {
            return WelcomeScreen();
          }
        }
      },
    );
  }
}
