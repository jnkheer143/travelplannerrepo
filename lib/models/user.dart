
class UserModel {
  final String userID;
  final String firstName;
  final String email;
  final String profilePictureURL;

  UserModel({
    this.userID,
    this.firstName,
    this.email,
    this.profilePictureURL,
  });

  Map<String, Object> toJson() {
    return {
      'userID': userID,
      'firstName': firstName,
      'email': email == null ? '' : email,
      'profilePictureURL': profilePictureURL,
      'appIdentifier': 'flutter-onboarding'
    };
  }

  factory UserModel.fromJson(Map<String, Object> doc) {
    UserModel user = new UserModel(
      userID: doc['userID'],
      firstName: doc['firstName'],
      email: doc['email'],
      profilePictureURL: doc['profilePictureURL'],
    );
    return user;
  }


}
