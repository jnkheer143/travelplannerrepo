import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onboarding_flow/ui/video/AfterVideo.dart';

class AppUtils {
  static nextScreen(BuildContext context, Widget child) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => child));
  }
}
