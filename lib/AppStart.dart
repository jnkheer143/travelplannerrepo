import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:onboarding_flow/ui/home/NewHome.dart';
import 'package:onboarding_flow/ui/AppScreens/root_screen.dart';
import 'package:onboarding_flow/ui/auth/sign_in_screen.dart';
import 'package:onboarding_flow/ui/auth/sign_up_screen.dart';
import 'package:onboarding_flow/ui/intro/walk_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

class AppStart extends StatefulWidget {
  State<AppStart> createState() => _AppStartState();
}

class _AppStartState extends State<AppStart> {
  final SharedPreferences prefs;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  _AppStartState({this.prefs});
  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {

    _firebaseMessaging.getToken().then((token) async {

      print("Main Token "+ token);

    });
    _firebaseMessaging.configure(

      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        print("Notification "+message.toString());
//
        final dynamic notification = message['notification'];
        _showNotification(notification["title"], notification["body"],
            notification["corname"],true);

      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );

    var initializationSettingsAndroid =
    AndroidInitializationSettings('launch_background');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }
  Future _showNotification(String requesttype , String body,String name,bool type) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    String trendingNewsId = '5';
    await flutterLocalNotificationsPlugin.show(
        0, requesttype, body,
        platformChannelSpecifics,
        payload: trendingNewsId);
  }
  Future onSelectNotification(String payload) async {

  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/walkthrough': (BuildContext context) => new WalkthroughScreen(),
        '/signin': (BuildContext context) => new SignInScreen(),
        '/signup': (BuildContext context) => new SignUpScreen(),
        '/main': (BuildContext context) => new NewHome(true),
      },
      theme: ThemeData(
        primaryColor: Colors.white,
        primarySwatch: Colors.grey,
      ),
      home: HandleState(),
    );
  }


}